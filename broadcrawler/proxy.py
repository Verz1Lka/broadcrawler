# -*- coding: utf-8 -*-
import random
import base64


class RandomProxy(object):
    def process_request(self, request, spider):

        if 'proxy' in request.meta:
            return

        if spider.proxies:
            proxy = random.choice(spider.proxies).split(':')
            if len(proxy) == 4:
                request.meta['proxy'] = 'http://' + proxy[0] + ':' + proxy[1]
                proxy_user_pass = proxy[2] + ':' + proxy[3]
                encoded_user_pass = base64.encodestring(proxy_user_pass).replace('\n', '')
                request.headers['Proxy-Authorization'] = 'Basic ' + encoded_user_pass
            if len(proxy) == 2:
                request.meta['proxy'] = 'http://' + proxy[0] + ':' + proxy[1]

        if spider.user_agents:
            user_agent = random.choice(spider.user_agents)
            request.meta['headers']['User-Agent'] = user_agent

        return

