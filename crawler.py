# -*- coding: utf-8 -*-

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings


class BroadCrawler:
    links = {}

    def __init__(self, domains=[], proxies=[], user_agents=[]):
        self.domains = domains
        self.proxies = proxies
        self.user_agents = user_agents
        return

    def start(self):
        process = CrawlerProcess(get_project_settings())
        for domain in self.domains:
            self.links[domain] = [[], []]
            sitemap_urls = ['http://{}/robots.txt'.format(domain), 'http://{}/sitemap.xml'.format(domain)]
            process.crawl('crawl', sitemap_urls=sitemap_urls, domain=domain, links=self.links[domain],
                          proxies=self.proxies, user_agents=self.user_agents)
            process.start() # the script will block here until the crawling is finished
        return

    def get_internal(self, domain):
        return self.links.get(domain, [None, None])[0]

    def get_external(self, domain):
        return self.links.get(domain, [None, None])[1]
