# -*- coding: utf-8 -*-

from crawler import BroadCrawler

domains = ['realpython.com']  # list for domains
proxies = ['127.0.0.1:8123', '23.88.95.141:80:skyrocket:skyrocket1']  # list for proxies
user_agents = []  # list for user-agents (default is mozilla, check settings.py)

crawler = BroadCrawler(domains=domains, proxies=proxies, user_agents=user_agents)  # init crawler
crawler.start()  # this blocking process

# print results
print 'INTERNAL: '
print crawler.get_internal(domains[0])  # method for get internal links
print 'EXTERNAL: '
print crawler.get_external(domains[0])  # method for get external links
